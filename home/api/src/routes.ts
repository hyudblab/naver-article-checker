import { articleGetAllAction } from "./controller/ArticleGetAllAction"
import { UserLogin } from './controller/UserLogin'

export const AppRoutes = [
    {
        path: "/",
        method: "get",
        action: articleGetAllAction
    },
    {
        path: "/login/:username",
        method: "get",
        action: UserLogin
    },
    {
        path: "/articles",
        method: "get",
        action: articleGetAllAction
    }
]