import 'reflect-metadata'
import { createConnection } from 'typeorm'
import Koa from 'koa'
import Router from 'koa-router'
import bodyParser from 'koa-bodyparser'
import { AppRoutes } from './routes'

type RouteMethod = {
  [key: string]: Function
}

async function main() {
  await createConnection()
  const app = new Koa()
  const router = new Router({
    prefix: '/api'
  })
  const routeMethod: RouteMethod = {
    post: router.post,
    get: router.get,
    put: router.put,
  }

  AppRoutes.forEach(route => routeMethod[route.method].call(router, route.path, route.action))

  app.use(bodyParser())
  app.use(router.routes())
  app.use(router.allowedMethods())
  app.listen(5000)

  console.log("Koa running on 5000")

}

main()