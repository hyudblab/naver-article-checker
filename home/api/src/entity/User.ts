import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm'
import { Checklist } from './Checklist'

@Entity("naver_check_user")
export class User {
    @PrimaryGeneratedColumn()
    uid!: number

    @Column()
    email!: string

    @Column()
    name!: string

    @Column()
    password!: string

    @OneToMany(type => Checklist, Checklist => Checklist.uid)
    checklists!: Checklist[]
}