import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm'
import { Article } from './Article'
import { User } from './User'

@Entity("naver_check_answer")
export class Checklist {
    @PrimaryGeneratedColumn()
    uid!: number

    @Column()
    email!: string

    @Column()
    name!: string

    @Column()
    password!: string

    @Column()
    xpath!: string

    @ManyToOne(type => User, user => user.checklists)
    user!: User

    @ManyToOne(type => Article, article => article.checklists)
    article!: Article
}