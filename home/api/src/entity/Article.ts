import { Entity, PrimaryColumn, Column, OneToMany } from 'typeorm'
import { Checklist } from './Checklist'


@Entity("naver_article_sample")
export class Article {
    @PrimaryColumn("int")
    id!: number

    @Column("int")
    aid_naver!: number

    @Column()
    title!: string

    @Column()
    content!: string

    @Column("date")
    date!: Date

    @Column()
    url_naver!: string

    @Column()
    url_origin!: string

    @Column()
    subcategory_naver!: string

    @Column()
    category_naver!: string

    @Column()
    press!: string

    @Column()
    pdf!: string

    @Column()
    num_comment!: number

    @Column()
    content_block_tag!: string

    @Column()
    content_block_attr!: string

    @Column()
    similarity!: number

    @Column()
    text_count!: number

    @Column()
    content_block_body_text!: string

    @OneToMany(type => Checklist, checklist => checklist.article)
    checklists!: Checklist[]
}