import { Context } from 'koa'
import { getManager } from 'typeorm'
import { Article } from '../entity/Article'

export async function articleGetAllAction(ctx: Context) {
    const articleRepo = getManager().getRepository(Article)

    const article = await articleRepo.findOne()
    ctx.body = article
    console.log(ctx.request)
    console.log(ctx.response)
}