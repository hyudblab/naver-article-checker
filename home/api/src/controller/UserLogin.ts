import { Context, Next } from 'koa'
import { getManager } from 'typeorm'
import { User } from '../entity/User'

export async function UserLogin(ctx: Context) {
    const userRepo = getManager().getRepository(User)
    console.log(ctx.params)

    const user = await userRepo.findOne({ name: ctx.params.username })
    if (user) {
        ctx.body = 'Hello! ' + user.name
        console.log("uid", user.uid)
    }
    else {
        ctx.body = "User Not Found"
    }
}